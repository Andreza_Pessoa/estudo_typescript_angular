export class Cliente {
    //atributos
    private _nome: string;
    private _cpf: string;
    private _rg: string;
    private _tel: string;

    //constructor
    constructor(nome: string, cpf: string, rg: string, tel: string) {

        this.nome = nome;
        this.cpf = cpf;
        this.rg = rg;
        this.tel = tel;

    }
    //metodos de acesso (getter e setter)
    public get nome(): string {
        return this._nome;
    }
    public set nome(nome: string) {
        this._nome = nome;
    }

    public get cpf(): string {
        return this._cpf
    }
    public set cpf(cpf: string) {
        this._cpf = cpf;
    }

    public get rg(): string {
        return this._rg
    }
    public set rg(rg: string) {
        this._rg = rg;
    }

    public get tel(): string {
        return this._tel
    }

    public set tel(tel: string) {
        this._tel = tel;
    }

}