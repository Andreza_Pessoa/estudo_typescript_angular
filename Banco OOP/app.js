"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express = require("express");
var jwt = require("jsonwebtoken");
var SECRET = "SohEuSei";
//importar rotas 
var Conta_1 = require("./Conta");
var Cliente_1 = require("./Cliente");
var app = express();
var porta = 3001;
app.listen(porta, function () {
    console.log("Servidor rodando na porta " + porta);
});
//convertendo os dados do corpo da requisição para JavaScript Object  
app.use(express.urlencoded({ extended: false }));
app.use(express.json());
function verificaUser(req, resp, next) {
    var token = req.header("x-access-token");
    jwt.verify(token, SECRET, function (err, decoded) {
        if (err) {
            return resp.status(401).end();
        }
        //req.dec = decoded.xxx
        next();
    });
}
//--------------------------------------------------
app.get("/", function (req, resp) {
    resp.json({ msn: "Chegou na primeira rota" });
});
///-----------------------------------------------------
// app.post("/conta", function (req, resp) {
//     var conta = new Conta(`${req.body.numConta}`,`${req.body.saldo}`,`${req.body.limite}`, `${req.body.tipo}`, `${req.body.senha}`)
//     resp.json({
//         "Status": "200",
//         "mensagem": "conta registrada com sucesso",
//         "exibir": conta
//     })
// })
//----------------------------------------------
app.post("/cliente", function (req, resp) {
    var conta = new Conta_1.Conta(1000, 3995, 50000, 4000, 200);
    var cliente = new Cliente_1.Cliente("" + req.body.nome, "" + req.body.cpf, "" + req.body.rg, "" + req.body.endereco, conta);
    console.log(cliente);
    resp.json({
        "Status": "200",
        "mensagem": "Cliente registrado com sucesso",
        "exibir": cliente
    });
});
// app.post("/banco", function (req, resp) {
//     var banco = new Banco(`${req.body.nome}`, `${req.body.cnpj}`, `${req.body.agencia}`, clientes: [])
//     console.log(banco);
//     resp.json({
//         "Status": "200",
//         "mensagem": "Banco registrado com sucesso",
//         "exibir": banco
//     })
// })
///-----------------------------------------------------
app.post("/cadastrar", function (req, resp) {
    if (req.body.user == "XPTO" && req.body.pass === 1234) {
        //gerar token - payload
        var token = jwt.sign({ xxx: req.body.user }, SECRET, { expiresIn: 200 });
        return resp.json({ auth: true, token: token });
    }
    resp.status(401).end();
});
