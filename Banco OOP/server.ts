
import *  as express from "express"

//import * as jwt from "jsonwebtoken"

//const SECRET = "SohEuSei"

//importar rotas 
import { Conta } from "./Conta";
import { Cliente } from "./Cliente";
import { Banco } from "./Banco";

const app = express()

const porta = 3001
app.listen(porta, function () {
    console.log("Servidor rodando na porta " + porta);
})

//convertendo os dados do corpo da requisição para JavaScript Object  
app.use(express.urlencoded({ extended: false }))
app.use(express.json())

var conta: Conta
var clientes = []
var cliente: Cliente
var banco: Banco


//--------------------------------------------------

app.get("/", function (req, resp) {
    resp.json({ msn: "Chegou na primeira rota" })
})

///-----------------------------------------------------

app.post("/conta", function (req, resp) {

    var numConta: number = Number(req.body.numConta)
    var saldo: number = Number(req.body.saldo)
    var limite: number = Number(req.body.limite)
    var tipo: number = Number(req.body.tipo)
    var senha: number = Number(req.body.senha)

    conta = new Conta(numConta, saldo, limite, tipo, senha)
    // console.log(conta);


    resp.json({
        "Status": "200",
        "mensagem": "conta registrada com sucesso",
        "exibir": conta
    })
})

//----------------------------------------------

app.post("/cliente", function (req, resp) {


    cliente = new Cliente(`${req.body.nome}`, `${req.body.cpf}`, `${req.body.rg}`, `${req.body.endereco}`, conta)
    clientes.push(cliente)


    resp.json({
        "Status": "200",
        "mensagem": "Cliente registrado com sucesso",
        "exibir": cliente
    })

})

//------------------------------------------------------

app.post("/banco", function (req, resp) {

    var agencia: number = Number(req.body.agencia)
    console.log(banco);


    banco = new Banco(`${req.body.nome}`, `${req.body.cnpj}`, agencia, clientes)
    console.log(banco);
    resp.json({
        "Status": "200",
        "mensagem": "Banco registrado com sucesso",
        "exibir": banco
    })
})


///-----------------------------------------------------


// app.get("/pesquisa/:cod", function (req, resp) {

//     var cod: number = Number(req.params.cod)
//     resp.json({
//         "Banco": banco.getCliente(cod)
//     })

// })

//----------------------------------------------------

app.get("/pesquisa/:cpf/:senha", function (req, resp) {
    var cpf: string = req.params.cpf
    var senha: number = Number(req.params.senha)


    for (let i = 0; i < clientes.length; i++) {
        if (banco.clientes[i].cpf == cpf && banco.clientes[i].conta.senha == senha) {
            resp.json({
                "saldo": banco.clientes[i].conta.saldo
            })
        }
    }
})


//---------------------------------------------------------

app.get("/pesquisa/:cpf/:senha/:saque", function (req, resp) {
    var cpf: string = req.params.cpf
    var senha: number = Number(req.params.senha)
    var saque: number = Number(req.params.saque)



    for (let i = 0; i < clientes.length; i++) {
        if (banco.clientes[i].cpf == cpf && banco.clientes[i].conta.senha == senha) {
            banco.clientes[i].conta.sacar(saque)
            resp.send(`Foi realizado um saque de ${saque} / saldo atual: ${banco.clientes[i].conta.saldo}`)

        } else {
            resp.send("dados incorretos")
        }
    }
})

//----------------------------------------------------------------------

app.get("/pesquisa/:cpf/:senha/:valor/:cod", function (req, resp) {
    var cpf: string = req.params.cpf
    var senha: number = Number(req.params.senha)
    var valor: number = Number(req.params.valor)
    var cod: number = Number(req.params.cod)


    for (let i = 0; i < clientes.length; i++) {
        if (banco.clientes[i].cpf == cpf && banco.clientes[i].conta.senha == senha) {
            if (cod == 1) {
                resp.send(banco.clientes[i].conta.sacar(valor))
            } else if (cod == 2) {
                resp.send(banco.clientes[i].conta.depositar(valor))
            } else {
                resp.send(`Opção invalida!`)
            }
        } else {
            resp.send(`Dados incorretos!`)
        }

    }
})
