import { Conta } from "./Conta";
import { Cliente } from "./Cliente";
import { Banco } from "./Banco";

var conta = new Conta(1000,3995,50000,4000,200)
var cliente = new Cliente("andreza", "000.000.000-12", "4422244", "rua 1234", conta)

var banco = new Banco ("brasil", "12345677854", 123, [])

// var cliente : Cliente;
// var banco : Banco;
// var conta: Conta;

// conta.depositar(100)
// console.log(conta.depositar);

// conta.sacar(900)
// console.log(conta.saldo);


console.log(conta.sacar(55000));
console.log(conta.sacar(3999));
console.log(conta.depositar(-30));
console.log(cliente);


banco.adicionarCliente(cliente)
console.log(banco);

console.log(banco.getCliente(0).cpf);

// var clienteUI = {
//     nome: "andreza",
//     cpf: "000.000.000-12",
//     rg: "4422244",
//     endereco: 'rua 1234',
//     
// }
// cliente = new Cliente(clienteUI.nome,clienteUI.cpf,clienteUI.rg,clienteUI.endereco,conta)

// var clienteUI2 = {
//     nome: "isabela",
//     cpf: "530.250.060-22",
//     rg: "6123284",
//     endereco: 'rua antonio',
//    
// }
// cliente = new Cliente(clienteUI2.nome,clienteUI2.cpf,clienteUI2.rg,clienteUI2.endereco,conta)

// var bancoUI ={
//     nome: "brasil",
//     cnpj: "12345677854",
//     agencia: 123,
//     clientes: []
// }
//  banco = new Banco(bancoUI.nome,bancoUI.cnpj,bancoUI.agencia,bancoUI.clientes)

// var contaUI = {
//     numConta :107080,
//     saldo: 3000,
//     tipo: 4000,
//     senha: 2090
// }

// conta = new Conta(contaUI.numConta,contaUI.saldo,contaUI.tipo,contaUI.senha)


