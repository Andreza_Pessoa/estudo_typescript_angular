
export class Conta {

    private _numConta: number;
    private _saldo: number;
    private _limite: number;
    private _tipo: number;
    private _senha: number;

    constructor(numConta: number, saldo: number, limite: number,tipo: number, senha: number) {
        this.numConta = numConta;
        this.saldo = saldo;
        this.limite = limite;
        this.tipo = tipo;
        this.senha = senha;
    }

    public get numConta(): number {
        return this._numConta;
    }
    public set numConta(value: number) {
        this._numConta = value;
    }


    public get saldo(): number {
        return this._saldo;
    }
    public set saldo(value: number) {
        this._saldo = value;
    }

    public get limite(): number {
        return this._limite;
    }
    public set limite(value: number) {
        this._limite = value;
    }

    public get tipo(): number {
        return this._tipo;
    }
    public set tipo(value: number) {
        this._tipo = value;
    }

    public get senha(): number {
        return this._senha;
    }
    public set senha(value: number) {
        this._senha = value;
    }


    public depositar(depositar : number): number {
        var depositar: number 
        this.saldo += depositar
        return this.saldo
    }


    public sacar(sacar: number): string {
        if(sacar > 0 && sacar <= (this._saldo + this._limite)){
            this._saldo = this._saldo - sacar;
            return `Foi realizada uma retirada de R$ ${sacar}, o saldo na conta é  R$ ${this._saldo}`
        }else{
            if(sacar < 0){
                return `Você tentou sacar R$ ${sacar}, apenas valores positivos`
            }else{
                return `Voce tentou sacar um valor maior do que o disponivel.\n
                Valor disponivel R$ ${this._saldo + this._limite}, sendo R$ ${this._saldo}, o seu saldo com cheque especial é R$ ${this._limite}`
            }
        }
    }

}