import { Conta } from "./Conta";

export class Cliente {
    private _nome: string;
    private _cpf: string;
    private _rg: string;
    private _endereco: string;
    private _conta: Conta;
   

    constructor(nome: string, cpf: string, rg: string, endereco: string, conta: Conta){
        this.nome = nome;
        this.cpf = cpf;
        this.rg = rg;
        this.endereco = endereco;
        this.conta = conta;
    }
    public get nome(): string {
        return this._nome;
    }
    public set nome(value: string) {
        this._nome = value;
    }

    
    public get cpf() {
        return this._cpf
    }
    public set cpf(cpf: string) {
        if (this.validaCpf(cpf)) {
            this._cpf = cpf
        }else{
            throw "erro ao validar cpf"
        }
    }

    public get rg(): string {
        return this._rg;
    }
    public set rg(value: string) {
        this._rg = value;
    }

    public get endereco(): string {
        return this._endereco;
    }
    public set endereco(value: string) {
        this._endereco = value;
    }

    public get conta(): Conta {
        return this._conta;
    }
    public set conta(value: Conta) {
        this._conta = value;
    }

    private validaCpf(cpf: string) {
        if (cpf == "") {
            return false
        } else {
            return true
        }
    }
}