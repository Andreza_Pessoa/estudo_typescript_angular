"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Banco = void 0;
var Banco = /** @class */ (function () {
    function Banco(nome, cnpj, agencia, clientes) {
        this.nome = nome;
        this.cnpj = cnpj;
        this.agencia = agencia;
        this.clientes = clientes;
    }
    Object.defineProperty(Banco.prototype, "nome", {
        get: function () {
            return this._nome;
        },
        set: function (value) {
            this._nome = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Banco.prototype, "cnpj", {
        get: function () {
            return this._cnpj;
        },
        set: function (value) {
            this._cnpj = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Banco.prototype, "agencia", {
        get: function () {
            return this._agencia;
        },
        set: function (value) {
            this._agencia = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Banco.prototype, "clientes", {
        get: function () {
            return this._clientes;
        },
        set: function (value) {
            this._clientes = value;
        },
        enumerable: false,
        configurable: true
    });
    Banco.prototype.adicionarCliente = function (cliente) {
        this._clientes.push(cliente);
        // return this.clientes
    };
    Banco.prototype.getCliente = function (cod) {
        var cod;
        return this._clientes[cod];
    };
    return Banco;
}());
exports.Banco = Banco;
