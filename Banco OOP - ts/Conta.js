"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Conta = void 0;
var Conta = /** @class */ (function () {
    function Conta(numConta, saldo, limite, tipo, senha) {
        this.numConta = numConta;
        this.saldo = saldo;
        this.limite = limite;
        this.tipo = tipo;
        this.senha = senha;
    }
    Object.defineProperty(Conta.prototype, "numConta", {
        get: function () {
            return this._numConta;
        },
        set: function (value) {
            this._numConta = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Conta.prototype, "saldo", {
        get: function () {
            return this._saldo;
        },
        set: function (value) {
            this._saldo = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Conta.prototype, "limite", {
        get: function () {
            return this._limite;
        },
        set: function (value) {
            this._limite = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Conta.prototype, "tipo", {
        get: function () {
            return this._tipo;
        },
        set: function (value) {
            this._tipo = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Conta.prototype, "senha", {
        get: function () {
            return this._senha;
        },
        set: function (value) {
            this._senha = value;
        },
        enumerable: false,
        configurable: true
    });
    Conta.prototype.depositar = function (depositar) {
        var depositar;
        this.saldo += depositar;
        return this.saldo;
    };
    Conta.prototype.sacar = function (sacar) {
        if (sacar > 0 && sacar <= (this._saldo + this._limite)) {
            this._saldo = this._saldo - sacar;
            return "Foi realizada uma retirada de R$ " + sacar + ", o saldo na conta \u00E9  R$ " + this._saldo;
        }
        else {
            if (sacar < 0) {
                return "Voc\u00EA tentou sacar R$ " + sacar + ", apenas valores positivos";
            }
            else {
                return "Voce tentou sacar um valor maior do que o disponivel.\n\n                Valor disponivel R$ " + (this._saldo + this._limite) + ", sendo R$ " + this._saldo + ", o seu saldo com cheque especial \u00E9 R$ " + this._limite;
            }
        }
    };
    return Conta;
}());
exports.Conta = Conta;
